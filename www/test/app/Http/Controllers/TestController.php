<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Test;

class TestController extends Controller
{
    public function indexAction()
    {
        $data = (new Test())->tree();

        return view('get', ['data' => $data]);
    }
}
