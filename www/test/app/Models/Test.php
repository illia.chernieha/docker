<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Test extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    protected $table = 'tree';


    public function tree()
    {
        return DB::table('tree')->select()->get();
    }
}
